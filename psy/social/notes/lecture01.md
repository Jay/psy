# An Introduction to Social Psychology

Table of Contents
- [An Introduction to Social Psychology](#an-introduction-to-social-psychology)
- [What is Social Psychology](#what-is-social-psychology)
	- [General Psychology Arc](#general-psychology-arc)
		- [Why do people act differently](#why-do-people-act-differently)
		- [Social Factors involving](#social-factors-involving)
- [Relevant Experiments to Note](#relevant-experiments-to-note)
- [Experimental Approaches](#experimental-approaches)
	- [Descriptive Studies](#descriptive-studies)
		- [Single Case Study](#single-case-study)
		- ["Ex Post Facto" Study](#ex-post-facto-study)
	- [Correlation Studies](#correlation-studies)
		- [Cross Sectional Study](#cross-sectional-study)
	- [Semi Experiments](#semi-experiments)
		- [Quasi Experiment](#quasi-experiment)
		- [Field Experiment](#field-experiment)
	- [Real Experiments](#real-experiments)
- [Roots of Social Psychology](#roots-of-social-psychology)
	- [European Socialogy of the 19th Century](#european-socialogy-of-the-19th-century)
	- [American Behaviorism of the 20th Century](#american-behaviorism-of-the-20th-century)

---

# What is Social Psychology

How **thoughts, feelings & actions** are influenced by the **actual**, or **implied**, presence of one or more people.

## General Psychology Arc

Social Psychology focuses on the following enboldened topics:

### Why do people act differently

- Personality / Disposition
	- The qualities of the person
	- This is condiered a stable cause --> it does not change
- **Situation and Context**
- Time
	- Specifically, why actions differ dependent on the age


### Social Factors involving 

Type | Explanation | Example
---|---|---
**Social** | Other people are involved in shaping the context of the action | Girlfriend/Boyfriend
Non-Social | Environmental factors involved with shaping the context of one's actions | The lighting in a room

# Relevant Experiments to Note

Name | Goal | Methodology | Results
---|---|---|---
Triplett | Social Faciilitatin Effect | Test children alone and with partner | Children working in pairs would perform the task faster
Stanford Prison Experiment | Explain how "good" people perform attrocities | Randomly select jailors and prisonors; Put in Prison like scenario; Uniforms were given | Jailors performed psychological warfare on the prisoners to keep them in check, however methodology is flawed due to frequent interviews causing  social suggestions. Experiment was called off. Also, it wasn't really an experiment to begin with.
Watching Eye Effect | Is one more cooperative when the software has eyes on it | Transfer money as dictator to underlings game; 2 copies, one with eyes as desktop pic, one without; randomly select which computer is used for participants | People are more cooperative with eyes, transfer more money.

> Note that the Standord Prison Exp took place in a west coast, elite, liberal university in the '70s, where the anti-war movement was at its peak.

>The watching eye experiment is considered a good experiment, as it has randomly selected participant teams and very simple to control test variables, that are not changed. The results are also **statistically significant**.

# Experimental Approaches

![Pyramid](pics/l1forsch.png)

## Descriptive Studies

### Single Case Study

A **single** person is analyzed, so that one can draw **universal** principles. These should be used in order to develop concepts and hypotheses, as there is not enough data to set concrete psychological "laws".

### "Ex Post Facto" Study

A study that is done after the fact. This can be an event that has occured in the past. Note that the tester has not influenced the situation, hence has no control over the variables. It too should only be used to develop hypotheses.

## Correlation Studies

### Cross Sectional Study

A type of observational study that analyzes **data** from a **representative subset** of a population at a given time in order to obtain its results. The subjects are **not randomized** to exposure, but instead are observed in order to determine their exposure.

>For example, in a study trying to show that people who smoke (the attribute) are more likely to be diagnosed with lung cancer (the outcome), the cases would be persons with lung cancer, the controls would be persons without lung cancer (not necessarily healthy), and some of each group would be smokers. If a larger proportion of the cases smoke than the controls, that suggests, but does not conclusively show, that the hypothesis is valid.

## Semi Experiments

### Quasi Experiment

An emprical study that estimates the causal impact of an intervention w/o random assignment. Despite this, they still control variables in the same way the later methods do, making the environment "artificial".

### Field Experiment

An experiment that randomizes subjects, however fails to completly control all variables, as it is done out in the wild. The issue with this would be, that the results may be contaminated, that is impacted by external factors. 

Examples:

- Clinical trials of pharmaceuticals are one example of field experiments.
- Economists have used field experiments to analyze discrimination, health care programs, charitable fundraising, education, information aggregation in markets, and microfinance programs.
- Engineers often conduct field tests of prototype products to validate earlier laboratory tests and to obtain broader feedback.

## Real Experiments

A real experiment is conveived when the following 4 axioms are achieved

1. Targeted **manipulation** of the **independent variable**
2. **Consistency** of all other variables
3. **Randomized assignment** of people to test/control/placebo group
4. An **accurate**, **reliable** and **objective** way of measuring the **dependent variable**

> **Recap**
> 
> ![](pics/l1indep.png)
>
> `y` depends on the value of `x`, which is independent

Pros | Cons
--- | ---
Find a relationship between the dependent and independent vars | Focus on independent variable leaves out other factors
Relating a phenomenum with a given independent var | `Artificialness` of the experiment, due to the standardization of the test

# Roots of Social Psychology

## European Socialogy of the 19th Century

> Wasn't really put into too much depth

In the 19th century, many psychologists tried to explain the rationale behind social phenomena. LeBon for instance attempted to explain why mass demonstrations often ended in aggression and violence.

## American Behaviorism of the 20th Century

The second world war influenced this time period heavily. The Nazi attrocities up to that point were unimaginable for most of the world and psychologists had to grasp, how a modern nation such as Germany could fall to such lows.

The Black Box theory is one of the prevalent ways of thinking in this time.

![](pics/l1bbox.png)

Common topics to study included:

- Authoritarian Mindsets
- Mass Communication
- Identity, specifically in a group

Many consider the '50s, '60s and 70's the golden era of social psychology.