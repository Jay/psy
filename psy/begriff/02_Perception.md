# Perception

- [Perception](#perception)
    - [Tasks of Perception](#tasks-of-perception)
    - [Senses](#senses)
        - [Process](#process)
            - [Sensation](#sensation)
            - [Perceptional Organization](#perceptional-organization)
            - [Identification and Classification](#identification-and-classification)
    - [Psychophysics](#psychophysics)
        - [TL;DR](#tldr)
        - [Thresholds](#thresholds)
            - [Absolute Thresholds](#absolute-thresholds)
            - [Difference Threshold](#difference-threshold)
                - [Weber Constant](#weber-constant)
                - [Weber-Fechner Constant](#weber-fechner-constant)
        - [Methods to determine Thresholds](#methods-to-determine-thresholds)
    - [Recognition-by-components theory (Biedermann)](#recognition-by-components-theory-biedermann)
    - [Summary](#summary)

Perception is defined as a `general process to understand objects and events in one's surroundings`.

## Tasks of Perception

- Input of external "energy sources" via senses and to organize these into percepts
    - Percepts being objects that are perceived
- A physical object, known as a `distal stimulus` is mapped to a receptor, known as the `proximity stimulus`
    - The proximity stimulus will be processed and will be combined with other stimuli to create an event.

>Distal: situated away from the center of the body or from the point of attachment.

## Senses

![](/lectures/psy/begriff/_res/w053sense.png)

Senses are...

- perceived to function automatically and are available at anytime
- no capacity limit
- (normally) do not have any interpretation issues

It should be noted, that it is possible to trick these senses, such as through the use of optical illusions.

### Process

![](/lectures/psy/begriff/_res/w061sense.png)

#### Sensation

Automatic, subconscious analysis of the receptor's information, as well as the analysis of elemental characteristics. See [Psychophysics](#psychophysics) for more info.

#### Perceptional Organization

- Structure of an internal representation of a stimuli's information about the object and it's environment
- Integration of single characteristics into a larger object representation

#### Identification and Classification

- Objects, that draw attention to themselves, are identified and given meaning
    - Long term memory is used to store said info

## Psychophysics

The relations between subjective (psychological) and objective (physical objects) facts.

![](/lectures/psy/begriff/_res/w066pp.png)

Type | Explanation
---|---
Outer Psychophysics | Relationship between **measurable** stimuli that are triggered by external events.
Inner Psychophysics | Relationship between neural arousement-processes and the relevant experiences.

### TL;DR

- Using psychophysics one can measure subjective sensations
- Allows for the systematic examination between stimuli and the psychic feelings
- Weber discovered a method to show how the JND is different to the initial stimuli

### Thresholds

#### Absolute Thresholds

How great must the physical intensity of a stimuli be, so that it can be perceived.

This done by testing the stimuli:

1. Providing the relevant stimuli
2. Asking whether the stimuli is perceived
3. Changing the intensity of the stimuli

Examples include: Hearing test

#### Difference Threshold

A difference threshold, also known as a `Just Noticeable Difference`, is when one has a stimuli that is being perceived. How much does one need to alter the stimuli's intensity, so that one can notice the difference.

1. Providing a basis stimuli
2. Providing an altered stimuli
3. Asking the participant if he or she noticed a difference

##### Weber Constant

![](/lectures/psy/begriff/_res/w072diffthre.png)

##### Weber-Fechner Constant

![](/lectures/psy/begriff/_res/w076wfg.png)

### Methods to determine Thresholds

Method | Explanation
---|---
Herstellungsverfahren | The subject gets a differing stimuli intensity until the absolute or difference threshold is reached.
Grenzverfahren | The experimenter varies the stimuli intensity continuously until the tester is capable of feeling, that is, feeling the difference between the stimuli.
Konstanzverfahren | Pairs of stimuli are randomly given to the tester and the tester denotes whether or not he perceives / perceives a difference between the stimuli.

## Recognition-by-components theory (Biedermann)

All objects can be classified into 36 standard components, known as geons. When storing objects in memory, we remember these geons, which he believes are the elementary components of real world objects.

![](https://upload.wikimedia.org/wikipedia/commons/2/28/Breakdown_of_objects_into_Geons.png)

It is important to note the effect of context on the situation.

![](/lectures/psy/begriff/_res/w095context.png)

## Summary 

- Perception is a three part process
    - Sensory
    - Perceptual Organization
    - Re- and Identification
- On the sensory level, physical energy is converted into neural energy by the senses
- On the perception organization level, one organizes the senses into coherent parts, so that objects and patterns are identifiable
- On the identification and re-identification level, percepts are compared to what is stored in memory and compared with existing, known objects.