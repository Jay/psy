# Social Identity Theory

## Summary

- Four core elements of SIT
    - Social Categorisation
    - Social Identity
    - Social Distinction
    - Social Discrimination
- Real vs Social competition
- Negative social identity
- The Minimal Group Paradigm

## Four Core Elements of SIT

### Social Categorisation

#### Cognitive Processes

- Spontaneous categorisation of social and non-social objects
- Accenting of the differences between categories
    - Including their subcategories

#### Function of Categorisation

- Reduce Complexity
    - Easier to process
    - Easier to orientate
    - Easier to make decisions based on knowledge

#### Implications of Social Perspective

- Stereotyping
    - Members of a group will be assigned typical group characteristics, despire only being a member

### Social Identity

- Part of the concept of self
    - One identifies with a group
    - Ex. I am a Psychologist
- A positive social identify depends on a positive self esteem
- The social identity can differ in terms of importance depending on where one is
    - Being a metal head has little bearing in a Football Stadium
- Social situations differ depending on whether one decides to identify themselves by their group or as an individual

||Inter Personal Behavior | Inter Group Behavior
---|---|---
Categorical differing salience and relevance | No | Yes
Uniform behavior and mentality within the group | Low | High
Perception with out-group individuals | Individual and differing | Stereotype

### Social Distinctions

Individuals strive for a positive social identity. 

- Positive social identity results from comparison between one's own group and other groups
    - In a certain category, one's group is better than someone elses
- If the comparison is unfavorable, one might...
    - be incentivized to join a group with higher status
    - strive to change the group to have a higher status
- This is a common cause inter-group conflict