# Anti Social Behavior

## Definitions

Antisocial Behavior includes

- Aggression
    - Any form of behavior with the goal to damage/hurt another organism, that does not want to be damaged
- Violence
- Uncooperative behavior
- Norm-deviating behavior

Aggression can also be prototypical (physical and verbal) or non-prototypical (relational and indirect)

> Relational Aggression: Damage someone's social status

## Frustration Aggression Hypothesis

Aggression occurs when one experiences frustration. Frustration is a reaction to the experience of being prevented from reaching one's goals.

> Frustration produces instigations to a number of different types of
response, one of which is an instigation to some form of aggression.

## Berkowitz's Model

### 1. Trigger 

Aversive Stimuli, e.g. Frustration, Temperature, Noise

### 2. Psychic Processes

- motoric Reactions
    - Fight or Flight
- affective condition
    - anger or fear
- cognitive evaluation proces
    - what is going to happen as a result

### 3. Consequence

Aggression inducing stimuli increase the chance of an aggressive retaliation

## Learning Theories

Reinforcement Learning:

- Positive reinforcement
- Negative reinforcement
- Punishment

## Social Information Processing 

![](/lectures/psy/social/notes/pics/l91.png)

## General Aggression Model 

![](/lectures/psy/social/notes/pics/l92.png)

