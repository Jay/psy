# An Intro to Psychology

- [An Intro to Psychology](#an-intro-to-psychology)
    - [Definition](#definition)
        - [Old](#old)
            - [Experience](#experience)
            - [Actions](#actions)
        - [New](#new)
            - [Goals of Psychology](#goals-of-psychology)
            - [In Praxis](#in-praxis)
    - [Tasks and Goals of Psychology](#tasks-and-goals-of-psychology)
        - [Example: Neurosis](#example--neurosis)
    - [Bit of History](#bit-of-history)
        - [1879](#1879)
        - [1860](#1860)
    - [Structuralism in Psychology](#structuralism-in-psychology)
        - [Tasks of Psychology](#tasks-of-psychology)
        - [Examples of Association](#examples-of-association)
        - [Experiment](#experiment)
            - [Benefits](#benefits)
    - [Subsections of Psychology](#subsections-of-psychology)

## Definition

### Old

A science comprised or experience and actions.

#### Experience

Internal processes, that a person perceives, thinks, imagines or emotionally impacts.

- Memories of an event
- Fear
- Anger

#### Actions

All events, both intended and non-intended that a person does.

- Movement
- Reflexes
- Thinking process to solve problems

### New

#### Goals of Psychology

- The description and explanation of the functionality of the different brain-systems, that are relevant to the controlling and behavior
    - Localization of the behavior controlling subsystems of the brain
    - Description of the processes of each part
    - Description of the interactions between subsystems, whilst a certain action is performed.

#### In Praxis

... `applied psychology` is the usage of **scientifically based processes** to **influence human behavior**, so that one can improve the **quality of life** of a single person or group.

It is however important to consider the ethical impacts. One should only apply methods that:

- Have been proven to be effective
- That cause no side effects
- Are efficient.

The **scientific basis** of these processes, is very important for psychology. 

## Tasks and Goals of Psychology

- Describing
- Explaining
- Prediction
    - The prediction of experience and behavior (specifically of the brain)
- Control
    - Controlling the behavior, whilst still fulfilling scientific criteria

### Example: Neurosis

|    Task     |                                         Description                                         |                        Example                        |
| ----------- | ------------------------------------------------------------------------------------------- | ----------------------------------------------------- |
| Describing  | Relevant symptoms found in behavior                                                         | Fears                                                 |
| Explanation | Theory of the genesis of Neurosis,                                                          | The conditioning of fears                             |
| Prediction  | Successful prognosis of future behavior is the only concrete way of proving our explanation | During the process of deletion, the fear is removed   |
| Control     | Theoretical and practical methods of psychotherapy                                          | Reducing outbreaks through systematic desensitization |

## Bit of History

### 1879

Wilhelm Wundt founds the first psychological institution, that deals with the experimental research into psychological phenomena. This is the first step in differentiating psychology from philosophy.

### 1860

Gustav Theodor Fechner conducted the first psychological experiment that was published. His experiment proved that the psychic phenomena can be experimentally proven, as they can be deterministic.

## Structuralism in Psychology

Structuralism was predominantly found in the 19th century. It said, that all psychic contents can be dismantled into smaller sections, similar to how atoms work.

### Tasks of Psychology

1. `Components of consciousness` are to be found and separated into individual blocks. For example, one could get the base elements of: Imagination, Emotions, Elemental perception.
2. Find rules, by which these `components of consciousness` combine to form more complex behaviors.

The problem with this way of thought, is that not all psychological phenomena are fully known, thus cannot be fully understood, especially at that time. 

### Examples of Association

Stimulus | Association |             Type
-------- | ----------- | -----------------------------
Swallow  | Summer      | Spatial & temporal contiguity
Red      | Fire        | Similarity
Black    | White       | Contrast
Cold     | Ice         | Cause and Effect

### Experiment

Experiments were conducted by systematically changing the conditions, until a change in the reaction is noticed.

#### Benefits

- Objectivity
- Systematic variation of experiment variable

## Subsections of Psychology

![](/lectures/psy/begriff/_res/w033subsections.png)

Specific Psychological Perspectives | Explanation
---|---
General Psychology | Actual internal processes and states, that are causally connected, which in turn create behavior.
Biological Psychology | Biological fundamentals that impact a person's experiences and behavior.
Social Psychology | Social determining factors that impact a persons social behavior when dealing with a group/individual.
Development Psychology | The development of behavior controlling systems over one's lifetime (child to adult).
Personality Psychology | The differences between individuals and how they act differently in given situations.

Praxis Orientated Subsections | Explanation
---|---
Psychic Diagnosis | The measurement of individual differences (e.g. intelligence tests), as well as the diagnosis of behavioral anomalies (depression-, fear tests)
Clinical Psychology | The explanation, diagnosis and therapy involved in treating behavioral anomalies. This also includes the development of new therapy methods.
Economic Psychology | Application of psychology in the workplace to improve organization and efficiency.
Pedagogical Psychology | The development of methods to assist in school learning & parental upbringing. Also includes the use of therapy to treat learning disorders.

