# Interdependency Theory and Social Dilemmas

Goal: Explain how and why certain interdependency structures the choice of interaction partner and behaviorial impacts.

## Base Assumptions

- The interdependency between people is defined as the extent, to which one person's behavior influences another's
- Interdepency Theory, is a theory of social relationships
- Every social interaction occurs due to a change of resources (be it money, status, love, attention or time), in which one gains or loses (cost).
- If gains and loses are in the same dimensions, one can quantify these
    - Also can calculate profit
- A person strives to maximize one's own profit (Self Use Axiom)

Thus, every social interaction continues, if both partners view such as profitable (low cost, high reward). If one has multiple choices, one choses the one with the highest profit.

> This applies both in short term exchanges (trade) or long term exchanges (relationships).

## Comparison Level

- Expectation of profit from an interaction, in regards to a specific resource
- This expectation comes from personal experience, as well as observations form previous interactions
- If the profit is not as high as the CL, it is more likely to be phased out.

### Paradox Effect

Relationships that previously had a low CL at the beginning (high profit), will become in the long term more unattractive, as the CL level rises over time (less perceived profit).

## Comparison Level for Alternatives (CL Alt)

- Expectation of profit of other possible relationships (Market of Possibilities)
    - Includes possibility of not pursuing any relationship
- Is always relevant, if the profit dips below the CL (so expectation)
    - One is likely to jump-ship to another relationship (CL Alt) if it results in more profit
- Common explanation of why people stay in unhappy relationships
    - The CL Alt is not higher than the CL (the sex was worth it)
- Relationships with similar levels of profit are stabler, compared to one's with starkly differing profits

![](/lectures/psy/social/notes/pics/l3cl.png)

## Result Matrix

One can simulate interdependencies in a given interaction situation using a result matrix.

![](/lectures/psy/social/notes/pics/l3ex1.png)
![](/lectures/psy/social/notes/pics/l3ex2.png)

![](/lectures/psy/social/notes/pics/l3ex3.png)

In this case, the action of one spurs the action of the other. Assuming one reads, the other one will do so too. If the other one listens to music, it brings them no benefit, unless both listen to music.

### Prisoners Dilemma

![](/lectures/psy/social/notes/pics/l3pd.png)

From the perspective of person x

Box | Term | Explanation
---|---|---
A | Reward | If he is silent, he is rewarded for his silence by having both receive the least worse "equal" option
B | Sucker | If he is silent, but his partner confesses, he loses and is taken advantage of by Y
C | Temptation | Making use of Y's trust in you and getting free
D | Punishment | Both of you confess, thus both are punished heavily

In this dilemma, there is...

- A pro-social option (cooperation), that is only worth it if both choose to follow through with this
- A selfish option (defection), that is only worth it if the other is trusting in you
    - Obviously this choice is less moral, however on average, you will get a lesser punishment

Selfish behavior typically comes from...

1. The temptation of less punishment
2. The fear of being the sucker

## Social Dilemma

Every interaction situation where...

- There is a choice for each participating actor
- An interaction that one can use, but no one else (defection)
- The total profit of everyone else is reduced, if one person decides to defect (instead of everyone cooperating)

### Common Dilemmas

#### Give Some Game

![](/lectures/psy/social/notes/pics/l3gsg.png)

#### Trust Game 

![](/lectures/psy/social/notes/pics/l3tg.png)

#### Public Goods Game

The sum of what everyone invests is doubled and distributed evenly. Ideally, everyone puts in a joint amount of money (half typically) and all is well. Except if someone does not contribute, then he will stand to profit the most.

Some players would compensate, by investing more, but that also is not sensible long term.

This game is improved:

- Communicating with all participants about the dilemma
- Having accountability
    - Anonymous is just asking for issues
- Playing over multiple rounds
    - Expectation that one will cooperate if they are stuck in the same boat
- Altruistic Punishments, if one misbehaves

![](/lectures/psy/social/notes/pics/l3pgg.png)