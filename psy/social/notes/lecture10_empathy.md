# Empathy

## Terms

Term | German | Explanation
---|---|---
Pro-social Behavior | Prosoziales Verhalten | Behavior, that is executed, with the goal to benefit another person
Willingness to Help | Hilfsbereitschaft | Form of prosocial behavior, that involves helping a person in an emergency
Altuism | Altruismus | Prosocial behavior, that provides no benefit for the one executing it.
Cooperation | Kooperation | The willingness to invest own resources in a group project, which's successes results in profit for everyone involved.

## Social-biological Approach

## Decision Making Process

1. Will the event be perceived?
2. Will the event be interpreted as an emergency? (Pluralistic Ignorarnce)
3. Does one have a sense of personal responsibility to help? (Verantwortungsdiffusion)
4. Does the person see a possibility to help?
5. Does the person choose to help?

## Bystander Effect

The more people that are present in a situation, to less likely one is to help (someone else will do it).

## Cost-Use

If cost for helping is low and has a high social cost for not helping, one likes to help. If one however has the opposite, where they have no social pressure to do so and there is a high cost to helping, one would rather leave the scene, ignore it or deny it ever happened.

![](/lectures/psy/social/notes/pics/l10cu.png)

## Empathy-Altruism Hypothesis

If one has a high level of empathy, one is more likely to act altruistic and thus help. If one is egoistic, due to low levels of empathy, one will attempt to flee the situation, if the option is there.

![](/lectures/psy/social/notes/pics/l10eah.png)

## Negative State Relief

> Counter-argument to Empathy-Altruism Hypothesis

People only help if it improves their mood. If you tell someone, that thier mood cannot be changed (pill that freezes mood), then they are less likely to help.

## How to improve willingness to help

- Reduce ambiguity of situation
    - If someone is injured, say it out loud
- Reciprocal expectation should be improved
- Empathy with the one in need
- Make it clear, that helping is effective
- Create a norm, that prevents the passing of responsibilities
- Identification with the group
- Improve one's sense of "values"