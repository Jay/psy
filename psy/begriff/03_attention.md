# Attention

## Filter of Resources

One can perceive attention as a filter between what we receive as sensory inputs and that what is given to the brain.

![](/lectures/psy/begriff/_res/w104att.png)

### Overt vs Covert Orientating

#### Overt

Overt orientation is the act of selectively attending to an object over alternatives by moving the eyes in that given direction. One must further differentiate reflexive and controlled motion, the former being fast motions caused by sudden stimuli, whilst the later are slow and voluntary movements.

#### Covert

Covert orientation is the act of changing focus, without moving one's eyes. Attention to a specific object.

## TLDR

- Attention Models
    - Filter extraneous information
    - limited Resource
- Influence Factors
    - Bottom Up
        - Stimulus salience
        - New salience
        - Reward salience
    - Top Down
        - Expectation salience
- Different kinds of attention
    - Region-, object- and dimension based attention
- An impression of a complete and detailed surrounding is an illusion
    - Senses cannot handle that quantity of information
- Visual Search
- Posner Cueing Task
    - Test to assess attentional shifts