# Theory of Justice

## Theories to Know

Theory | TL;DR
---|---
Equity Theory | `(Oa/Ia) = (Ob/Ib)`; Inequity leads to dissatisfaction; All profit's from exchanges should be proportional
Mehrprinzipienansatz | Splitting Justice into `Equity`, `Equality` and `Needs`; Each Fulfills a different aspect
Criteria for Fair Process | `Consistent`, `Exact`, `Moral` and `Neutral` are the expected standards
Fair Process Effect | A just process is even with inequity perceived more positivly.
Reversed Fair Process Effect | When faced with a bad result, one is more happy with a unfair procedure, as they can blame the procedure.
Equality Motive Theory | The belief that eveyrone earns and gets, what they are supposed to.
Theory of Relative Deprivation | 
Relative Privalage |

## Equity Theory

- Proportionality Principle
    - If one is to profit from a exchange relationship, one expects that everyone profits proportionally to what they invested
        - If a third person is in charge of rewarding, it is expected that they also uphold said relation

### Inequity

If the outcome divided by the input of one relationship is not equal with another, inequity is present. 

```
Outcome A       Outcome B
---------  =!=  ---------
Input   A       Input   B
```

If the ratio of A is greater than that of B, then the inequity is in favor of A and vice versa.

#### Reduction of Inequity

##### Realistic Strategies

- Reduce own Input
    - Work Less
    - More Sick days
- Increase own Output
    - Ask for a raise
    - Steal from the company
- Increasing other's Input
    - Ask them to work more
        - Delegate work to them
- Reducing the other's Output
    - Demanding others to be paid less

##### Cognitive Strategies

- Inequity not in one's favor
    - Changing the comparison variable
        - Factor more in your favor
    - Changing the person who is compared
    - Ignoring the inequity
    - Justify the inequity
        - self deprivation
- Inequity that is in one's favor
    - Changing the comparison variable
    - Ignoring
    - Justification of the Equity
        - Input more

### Example Cases

#### Wages

- Underpaid workers are more likely to be less productive (slower, unmotivated, worse, stealing) and thus end up costing the company more
- Overpaid workers are more likely to invest more than they are paid for, to justify their wage

#### Couple

- Inequity between couples leads to dissatisfaction
    - Is not the only factor involved in this however

### Criticisms of the Equity Theory

- Formula can't deal with negative numbers
- How does one measure input and outputs
- Time scale
- What factor should one analyze?
- How is equity distributed

## Mehrprinzipienansatz

Distribution Principle | Social Context | Distal Goal | Type of Resource
---|---|---|---
Equity | Economic Dominated | Productivity | Money & Goods
Equality | Cooperation and Friendliness | Social Harmony | Information and Kindness 
Needs | Solidatiry, Community | Development | Love

## Procedural Equality

Standards of procedural equiality are...

- Consistency
- Accuracy
- Neutrality
- Correctability
- Representativity
- Orientated to moral standards

## Fair Process Effect

Even in processes with inequity, one is more likely to help out if the procedure was fair.

## Reverse Fair Process Effect

If a bad result occurs, you are happier if the procedure was unfair (blame the procedure, not yourself).

## Equality Motive Theory

- People believe that they live in a just world
    - Everyone gets what they are entitled to
- One perceives that the world is stabile and ordered
    - One is thus inclined to protect this mentality
- Inequality in said world thus creates cognitive dissonance
    - One can reduce this dissonance, by minimalizing the inequality (e.g. helping out) or using cognitive strategies (e.g. his own fault, there's no problem)

## Theory of Relative Deprivation

### 1976 Version with 5 Criteria

1. `P` notices, `O` owns object `X`
2. `P` would like to own `X`
3. `P` believes he as a right to `X`
4. `P` belives, that the ownership of `X` is not possible
5. `P` believes, it is his own fault, that he does not own `X`

### 1982 Version with 2 Criteria

1. `P` does not own object `X`, but would like to own it
2. `P` believes he has earned the right to own `X`

### Case Study: US Military

- In the Air Force, pay is good and promotions frequent
- Vice Versa for Military Police
- Assumption: AF is happier than MP
- Result: Opposite
    - Every person compares themselves to the people within their own group
    - Thus, those in the AF are more frustrated at not being promoted, compared to the MP where it is normal not to be promoted

### Runciman

![](/lectures/psy/social/notes/pics/l4rd.png)

### Existential Guilt

Occurs when...

- A benefits from B suffers
- A perceives his benefit as undeserved
- A perceives B's suffering as undeserved

Is strengthened by...

- A notices that his benefits directly caused B's issues
- A finds methods to reduce the discrepency between his gains and B's loses
    - A does not take advantage of these methods