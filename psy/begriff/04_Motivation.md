# Motivation

## Definitions of Motivation

1. Biological drives due to needs
2. Construct to explain the differences in behavior. Why people react differently in identical situations.
3. Explanation of observed actions due to inner states.
4. Assignment of responsibility of own and foreign actions.
5. Explanation of persistance and endurance in the pursuit of objectives. AKA, how much effort one puts into pursuing their goals compared to another.

## Sources of Motivation

Name | Origin | Explanation
---|---|---
Motivation due to urge | Woodworth, 1918 | A biological urge to fulfill a specific goal, which causes energy for said cause to be mobilized. A specific stimuli is the cause of the urge (hunger, sex, etc.). Fulfilling an urge is done by an instinct, who's aim is to reduce the urge.
Motivation through urges is a mandatory prerequisite for learning | Hull, 1940's | Motivation is based on tension. Reducing tension results in a positive anchoring of an mechanism. Any urge, regardless of strength, will impact an organisms actions, until said urge is satisfied.

