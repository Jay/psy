# Social Influences and Norms

## Summary

### Forms of Influences

- Suggestion
- Manipulation
- Propaganda
- Advertisement
- Seduction
- Persuasion
- Conviction
- Power and Authority

## Casual Social Influencing

Term | Explanation
---|---
Social Relief | Improvement of one's performance in the presence of others
Social Inhibition | Worsening of one's performance in the presence of others

### Triplett's Experiment

Triplett had an experiment to see how fast one could turn a spool. Sometimes there was a person present, other times not.

![](/lectures/psy/social/notes/pics/l7tripplet.png)

Whilst not very significant, having a person in the room does minimally improve the time taken for the task

### Zajonc

Zajonc came to the conclusion, that having other's present while doing easier `routine` activities, leads to improved performance due to a relief of dominant reactions (you know what you are doing).

The opposite occurs for non-dominant reactions, which leads to an inhibition of `new` or `complex` tasks.

## Social Norms

Term | Explanation
---|---
Descriptive Norms | Describes how people typically react
Prescriptive / injunctive Norms | Describe, how one should react (laws, Knigge)

It is important to differentiate these two, as what one **should do**, often differs from how the average human **typically** reacts.

### Function of Norms

- Decreasing insecurity
    - What is normal
- Coordinating Behavior
    - Traffic
- Fairness
    - What is a fair procedure
- Social Identity in a group

## Conformity in Groups

### Why does a person follow norms

Term | Explanation
---|---
Informational Influence | Desire to gather correct information to decide correctly
Normative Influence | Desire for social harmony and to fit in

Due to these desires, people in groups act more homogene than as individuals. This also impacts their perception, as well as their actions.

It was found through experiments that...

- People that are ... are more likely to conform
    - Doubt their own judgement
    - Afraid of Conflict
    - Fear of being judged
- One's culture plays a role in conformity
    - In Collective Cultures, conformity is stronger than in individualistic ones

### Minority Influence

It is possible for a minority to influence the majority. By consistently saying that a blue surface is green, is possible to influence one's perception of color.

Possible Real Life Topic: US Elections and the whole social media part could be seen as a further example of this.

## Obedience to Authority

The infamous Milgrim Experiment demonstrated that one would be willing to follow orders, if someone from authority told them to do so.

The experiment was expanded, to analyze the positioning of the learner, so that the test subject would have different levels of "interaction" with them.

![](/lectures/psy/social/notes/pics/l7auth.png)

The same was also done for different settings.

![](/lectures/psy/social/notes/pics/l7auth1.png)

And if others were present who are conforming.

![](/lectures/psy/social/notes/pics/l7auth2.png)

## Compliance

Strategy | Initial | End | Chance of Success
---|---|---|---
Foot in the Door | Small Favor | Change Favor to large | High
Door in the Face | Large Favor | Change Favor to small | Low
Low-ball Strategy | Deal with good conditions | Renegotiate to less favorable terms

## Non-Conformity

### Individual 

- Reactance
- Desire for autonomy
- Self Control
- Moral 
    - Sacred Values
- Civil Courage

>Reactance is a motivational reaction to offers, persons, rules, or regulations that threaten or eliminate specific behavioral freedoms. 

### Collective

- Relative Deprivation
- Relative Privilige
- Social Distinction
- Collective Efficacy

>In the sociology of crime, the term collective efficacy refers to the ability of members of a community to control the behavior of individuals and groups in the community.