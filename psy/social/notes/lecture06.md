# Attribution Theory

## Naive Action Analysis

![](/lectures/psy/social/notes/pics/l6naive.png)

## Cause Schemata

![](/lectures/psy/social/notes/pics/l6cause.png)

## Covariance Principle

People gather information about others, contexts, time and compare these, so as to create a causal opinion of a certain event.

These can be represented by a cube:

![](/lectures/psy/social/notes/pics/l6cube.png)

### Example Exam

Term | Dimension | Example: Person X
---|---
Person | How have other's done in this exam | Everyone passed the exam except X
Entity | How much effort have you put into other exams up to this point | X has other exams at the same time which he hasn't passed
Time   | How has one done in previous exams in the same subject | X has written multiple exams in this subject and has failed all of them

![](/lectures/psy/social/notes/pics/l6cotbl.png)

> Thus, there is a low consensus between people (from X's perspective), as at this time, everyone else has passed, except X

### Criticism

- Attributes may be assigned, despite not having the complete information regarding consensus, distinction and consistency
- People do not always analyze situations "rationally", thus may develop false causal attributions
- Attribution is influenced by personal motivations, thus is subject to biases that may not be accounted for

### Correspondence Distortion

TODO FINISH