# Experiments

## Realistic Conflict Theory

### Robbers Cave Study

- 22 Boys
- Aged 11-12
- Similar Backgrounds

#### Stages

1. Ingroups were formed, unaware of each other
2. Friction Phase: Competition for winner-takes-all type prizes
3. Integration state: Tensions were reduced via intergroup cooperative tasks

#### Results

- Hostile behavior is generated via competition for limited resources
- Contact itself with an outgroup is insufficient to cease hostilities
    - One must have positive, cooperative intergroup activities

#### Similar Experiments

Lutfy Diab repeated the experiment with a group of 18 boys. 2 Groups, 5 Christians and 4 Muslims. Fighting broke out between the groups and **not** because of religion.

## Autokinetic Effect

### Question

Does the point move, if so by how much?

### Answer

![](/lectures/psy/social/notes/pics/eAK.png)


## Solomon Asch - Conformity Experiment

### Premisis

One Test Subject, a group of fake test subjects.

### Faux Question

![](https://www.simplypsychology.org/lines.jpg)

Which line best represents X? (Test subject asked last)

### Real Question

If everyone says that the line is C instead of A, would the subject go along with it or would he stick with his own guns.

### Results 

Over the 12 critical trials, about 75% of participants conformed at least once, and 25% of participant never conformed.

Control Group: Less then 1% incorrect

### Reason

> When they were interviewed after the experiment, most of them said that they did not really believe their conforming answers, but had gone along with the group for fear of being ridiculed or thought "peculiar."...
> 
> ... Apparently, people conform for two main reasons: because they want to fit in with the group (normative influence) and because they believe the group is better informed than they are (informational influence).

## Moscovici et al. (1969) Blue-Green Study

### Aim

To investigate the effects of a consistent minority on a majority.

### Procedure

1. 2 Non-Colorblind people with 2 test
2. 36 Slides shown with different shades of blue
3. Identify color out loud

### Variations

1. Control group
2. Consistently saying green
3. 24:12 Green vs Blue

### Results

![](https://www.simplypsychology.org/moscovici.jpg)

### Criticism

- Is telling blue and green really true to life
- Only female students

## The Milgram Experiment

> Could it be that Eichmann and his million accomplices in the Holocaust were just following orders? Could we call them all accomplices?" (Milgram, 1974).

How far would one go in terms of electrocuting someone, especially if someone with authority tells them to do so? 

If one refused, the experimenter would read out a prompt to encourage them to continue.

![](https://www.simplypsychology.org/milgram.jpg)

> Prod 1: Please continue.
>
>Prod 2: The experiment requires you to continue.
>
>Prod 3: It is absolutely essential that you continue.
>
>Prod 4: You have no other choice but to continue.

### Results

All subjects continued on to the 300V level which was the "border" to damage. 2/3 even continued to the highest level of 450V.

![](https://www.simplypsychology.org/milgram-scale.jpg)

## Mere Exposure Effect

> Zajonc tested the mere-exposure effect by exposing Chinese characters for shorts amounts of time to two groups of individuals. 
> 
> The individuals were then told that these symbols represented adjectives and were asked to rate whether the symbols held **positive** or **negative** connotations. 
> 
> The symbols that had been **previously seen** by the test subjects were consistently rated more **positively** than those unseen. 